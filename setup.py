from setuptools import setup

setup(
    name='beets-rapc',
    version='0.1',
    description='beets plugin to support rating and play count tags',
    long_description=open('README.md').read(),
    author='Dave Buchan',
    author_email='bluebucket@bluebucket.org',
    url='https://gitlab.com/bluebucket/beets-rapc',
    license='GPL3',
    platforms='ALL',

    packages=['beetsplug'],

    install_requires=[
        'beets>=1.6.0'
    ],
)