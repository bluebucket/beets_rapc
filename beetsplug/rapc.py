import beets
from beets.plugins import BeetsPlugin
from beets import ui
from beets.util import syspath

from beets.dbcore import types
from beets.dbcore.types import FLOAT

from beets.library import Item

import mediafile
from mutagen.id3._frames import TXXX
from mutagen.id3._frames import POPM

RATING = 'rapc_rating'
PLAYCOUNT = 'rapc_playcount'

class RAPCPlugin(BeetsPlugin):
    
    def __init__(self):
        super(RAPCPlugin, self).__init__()

        self.config.add({
            # Should we automatically import any values we find?
            'auto': False,
        })

        # Add importing ratings to the import process
        #if self.config['auto']:
        #    self.import_stages = [self.imported]

        rapc_rating_field = mediafile.MediaField(
            VorbisRatingStorageStyle(_log=self._log),
            MP3RatingStorageStyle(_log=self._log),
            out_type=float,
            float_places=6
        )

        rapc_playcount_field = mediafile.MediaField(
            VorbisPlaycountStorageStyle(_log=self._log),
            MP3PlaycountStorageStyle(_log=self._log),
            out_type=float,
            float_places=6
        )

        self.add_media_field(RATING, rapc_rating_field)
        self.add_media_field(PLAYCOUNT, rapc_playcount_field)

    def commands(self):
        """
        Return the "rapc" ui subcommand.
        """

        cmd = ui.Subcommand('rapc', help='manage ratings and play counts')
        cmd.func = lambda lib, opts, args: self.handle_items(
            lib, lib.items(ui.decargs(args)), opts)
        
        cmd.parser.add_option(
            '-t', '--tags-update', 
            action='store_true', 
            default=False,
            help='Update file tags from Library',
        )
        cmd.parser.add_option(
            '-u', '--update', 
            action='store_true', 
            default=False,
            help='Update Library from file tags',
        )

        return [cmd]
    
    def handle_items(self, lib, items, opts):
        if len(items) == 0:
            self._log.warning("no items found.")
            
        for item in items:
            self.handle_item(item, opts)

    def handle_item(self, item: Item, opts):
        if opts.update:
            self.update_library(item, opts)
        elif opts.tags_update:
            self.update_tags(item, opts)
        else:
            self.display_item(item, opts)         

    def display_item(self, item: Item, opts):
        self._log.info(u'{0}:', item)

        if RATING in item:
            self._log.info(u'    Rating    : {0}', item.rapc_rating)
        else:
            self._log.warning(u'    Rating    : None')

        if PLAYCOUNT in item:
            self._log.info(u'    Play Count: {0} ', item.rapc_playcount)
        else:
            self._log.warning(u'    Play Count: None')

    def update_library(self, item: Item, opts):
        self._log.debug(u'Getting tags for {0}', item)
        mf = mediafile.MediaFile(syspath(item.path))
        
        item.rapc_rating = getattr(mf, RATING, None)
        item.rapc_playcount = getattr(mf, PLAYCOUNT, None)

        #rating = item.fmps_rating if 'fmps_rating' in item else None
        self._log.debug(u'  Rating    : "{0}"', item.rapc_rating)
        self._log.debug(u'  Play Count: "{0}"', item.rapc_playcount)

        item.store()
        
        self._log.debug(u'Applied tags from {0} to Library', item)

    def update_tags(self, item: Item, opts):
        self._log.debug(u'Write tags for {0}', item)

        if item.try_write:
            item.write()

class VorbisRatingStorageStyle(mediafile.StorageStyle):
    """
    Ratings SHALL be a float value between 0.0 and 1.0, inclusive. 0.0 SHALL be the
    lowest possible rating; 1.0 is the highest possible rating.

    Ratings SHOULD only be rounded when necessary, in order to increase cross-player
    compatibility.
    """

    TAGS = [ 'FMPS_RATING', 'RATING' ]

    def __init__(self, **kwargs):
        self._log = kwargs.get('_log')
        super(VorbisRatingStorageStyle, self).__init__(self.TAGS)

    def get(self, mutagen_file):
        values = []
        for tag in self.TAGS:
            self._log.debug(u'Getting {0} tag', tag)
            if mutagen_file.get(tag) is not None:
                
                value = float(mutagen_file.get(tag)[0])
                if tag == 'RATING':
                    value = value / 100

                values.append(value)

        if values:
            return max(values)

        return None

    def set(self, mutagen_file, value):
        if value is not None:
            self._log.debug(u'Setting {0} tag', self.TAGS[0])
            mutagen_file[self.TAGS[0]] = str(value)

            self._log.debug(u'Setting {0} tag', self.TAGS[1])
            value = int(float(value) * 100)
            mutagen_file[self.TAGS[1]] = str(value)
    
class VorbisPlaycountStorageStyle(mediafile.StorageStyle):
    """
    Playcounts MUST be a float value not less than 0.0. 0.0 is valid and means unplayed.

    The maximum value SHALL be 0.000001 less than the largest value able to be stored
    in a 32-bit unsigned integer: 4,294,967,294.999999. This is so that playcount values
    can be rounded to an integer equivalent, if necessary (e.g. for display to the user).
    """

    TAGS = [ 'FMPS_PLAYCOUNT', 'PLAYCOUNT' ]

    def __init__(self, **kwargs):
        self._log = kwargs.get('_log')
        super(VorbisPlaycountStorageStyle, self).__init__(self.TAGS)

    def get(self, mutagen_file):
        values = []
        for tag in self.TAGS:
            self._log.debug(u'Getting {0} tag', tag)
            if mutagen_file.get(tag) is not None:
                
                value = float(mutagen_file.get(tag)[0])
                values.append(value)

        if values:
            return max(values)

        return None

    def set(self, mutagen_file, value):
        if value is not None:
            for tag in self.TAGS:
                self._log.debug(u'Setting {0} tag', tag)
                mutagen_file[tag] = str(value)
        
class MP3RatingStorageStyle(mediafile.MP3StorageStyle):
    """

    """
    FRAME = 'POPM'

    def __init__(self, **kwargs):
        self._log = kwargs.get('_log')
        super(MP3RatingStorageStyle, self).__init__(self.FRAME)

    def get(self, mutagen_file):
        values = []
        self._log.debug(u'Getting MP3 tag')
        popm_frame = mutagen_file.tags.getall('POPM')
        if popm_frame:
            values.append(float(popm_frame[0].rating / 255))

        fmps_frame = mutagen_file.tags.getall('TXXX:FMPS_Rating')
        if fmps_frame:
            values.append(float(fmps_frame[0].text[0]))

        if values:
            return max(values)

        return None

    def set(self, mutagen_file, value):
        if value is not None:
            mutagen_file['TXXX'] = TXXX(encoding=3, desc='FMPS_Rating', text=[value])

            rating_value = int(float(value) * 255)
            popm_frame = mutagen_file.tags.getall('POPM')

            if popm_frame:
                playcount_value = int(float(popm_frame[0].count))
            else:
                playcount_value = 0

            mutagen_file['POPM'] = POPM(email="", rating=rating_value, count=playcount_value)
    
class MP3PlaycountStorageStyle(mediafile.MP3StorageStyle):
    """

    """
    FRAME = 'POPM'

    def __init__(self, **kwargs):
        self._log = kwargs.get('_log')
        super(MP3PlaycountStorageStyle, self).__init__(self.FRAME)

    def get(self, mutagen_file):
        values = []
        self._log.debug(u'Getting MP3 tag')
        popm_frame = mutagen_file.tags.getall('POPM')
        if popm_frame:
            values.append(float(popm_frame[0].count))

        fmps_frame = mutagen_file.tags.getall('TXXX:FMPS_Playcount')

        if fmps_frame:
            values.append(float(fmps_frame[0].text[0]))

        if values:
            return max(values)

        return None

    def set(self, mutagen_file, value):
        if value is not None:
            mutagen_file['TXXX'] = TXXX(encoding=3, desc='FMPS_Playcount', text=[value])

            playcount_value = value
            popm_frame = mutagen_file.tags.getall('POPM')

            if popm_frame:
                rating_value = int(float(popm_frame[0].rating))
            else:
                rating_value = 0

            mutagen_file['POPM'] = POPM(email="", rating=rating_value, count=playcount_value)
